<?php

include_once('tp2-helpers.php');

# fonction à executer
function test(){
    $geop = geopoint(5.72752, 45.19102);
    $ApProches = proximiteTopN(dataFromCSV("borneswifi.csv"), $geop, 5);
    print_r($ApProches);
    printAPdistance($ApProches, $geop);
}
test();


##################################################################

#3. **Comptage PHP**
# récupérer le fichier sous forme d'un tableau
// $array = file($argv[1]);
// for($i = 0; $i < count($array); $i++)
// {
//     echo($array[$i]);
// }

##################################################################

#4. **Structure de données**. 
# Parser le fichier csv dans une structure de donnée pratique:
#  clés `name`, `adr`, `lon`, `lat`
function dataFromCSV($filepath){
    $file = fopen($filepath, "r");
    if ($file == FALSE) {
        echo "Can't open the csv file, quitting ..."; exit("\n");
    }
    $data = [];
        #Récupère les lignes du fichier csv une par une et les insère dans un tableau de données
    fgetcsv($file, 1000, ",");
    while (($line = fgetcsv($file, 1000, ",")) !== FALSE){
        $data[] = initAccesspoint($line);
    }
    fclose ($file);
    return $data;
}

##################################################################

#5. **Proximité**. 
    #Affiche tous les points d'accès et leur distance par rapport au geopoint passé en paramètre
    function printAPdistance($data, $geopoint){
        for($i=0; $i < count($data);$i++){
            $distance = distance(geopoint($data[$i]['lon'], $data[$i]['lat'] ), $geopoint);
            echo $data[$i]['name'], ' > ', $distance, "\n";
        }
    }

    #Affiche tous les points d'accès dont la distance au geopoint passé en paramètre est inférieure à $distanceMax
    function printAPdistanceMax($data, $geopoint, $distanceMax){
        for($i=0; $i < count($data);$i++){
            $distance = distance(geopoint($data[$i]['lon'], $data[$i]['lat'] ), $geopoint);
            if($distance < $distanceMax)
                echo $data[$i]['name'], ' > ', $distance, "\n";
        }
    }

    #Fonction retournant un tableau contenant tous les points d'accès dans la distance est inférieure à une distance passée en paramètre
    function getAPproximity($data, $geopoint, $distanceMax){
        $res = [];
        for($i=0; $i < count($data);$i++){
            $distance = distance(geopoint($data[$i]['lon'], $data[$i]['lat'] ), $geopoint);
            if($distance < $distanceMax)
                $res[] = $data[$i];
        }
        return $res;
    }

##################################################################

#6. **Proximité top N**. 
    function proximiteTopN($data, $geopoint, $N){
        $longs = array_column($data, 'lon', );
        $lats = array_column($data, 'lat');
        $res = [];
        for($i = 0; $i < count($longs); $i++)
           $res[] = distance(geopoint($longs[$i], $lats[$i]), $geopoint);
        array_multisort($res, SORT_DESC, $data);
        return array_slice($data, 0, $N);
    }








?>